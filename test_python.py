import unittest
from sample_python import adder

class TestSample(unittest.TestCase):
    def setUp(self):
        pass

    def test_adder(self):
        self.assertEqual(3, adder(1,2))

if __name__ == '__main__':
    unittest.main()
